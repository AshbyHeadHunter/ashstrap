<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.cosmedicwest
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


if (($this->error->getCode()) == '404') {
header('Location:'.$this->baseurl.'/404');
exit;
}


?>