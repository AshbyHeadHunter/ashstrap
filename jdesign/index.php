<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');


// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets





// Load optional rtl Bootstrap css and Bootstrap bugfixes
//JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);

// Add current user information
$user = JFactory::getUser();



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php 
	$minified = $this->params->get('minified');
	switch ($minified) {
	    case 0:
	        $doc->addStyleSheet('templates/'.$this->template.'/assets/css/template.css');
	        break;
	    case 1:
	        $doc->addStyleSheet('templates/'.$this->template.'/assets/css/template.min.css');
	        break;  
	}
	?>
	<style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
			

</head>

<body>
	<header role="banner"></header>

	<!-- NAV -->
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $this->baseurl; ?>"><?php echo $sitename; ?></a>
        </div>
        <div class="navbar-collapse collapse">
        	<?php if ($this->countModules('position-1')): ?>
				<jdoc:include type="modules" name="position-1" style="none" />
			<?php endif; ?>
            <?php if ($this->countModules('position-2')): ?>
				<jdoc:include type="modules" name="position-2" style="none" />
			<?php endif; ?>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
	<!-- END NAV -->
	<!--BANNER -->
	<?php if ($this->countModules('banner')): ?>
	<div class="jumbotron">
    	<div class="container">
			<jdoc:include type="modules" name="banner" style="xhtml" />
		</div>
	</div>
	<?php endif; ?>
	
	<div class="container">
		<?php if ($this->countModules('position-3')): ?>
			<div class="row">
				<jdoc:include type="modules" name="position-3" style="4cols" />
			</div>
		<?php endif; ?>
		<?php if ($this->countModules('position-4')): ?>
			<!-- Begin Sidebar -->
				<jdoc:include type="modules" name="position-4" style="3cols" />
			<!-- End Sidebar -->
		<?php endif; ?>
				
		<main id="content" role="main">
			<!-- Begin Content -->
			<jdoc:include type="message" />
			<jdoc:include type="component" />
			<?php if ($this->countModules('position-5')) : ?>
				<jdoc:include type="modules" name="position-5" style="none" />
			<?php endif; ?>
			<!-- End Content -->
		</main>
				
		<?php if ($this->countModules('position-6')) : ?>
		<div id="aside">
			<!-- Begin Right Sidebar -->
			<jdoc:include type="modules" name="position-6" style="3cols" />
			<!-- End Right Sidebar -->
		</div>
		<?php endif; ?>
		<!-- Footer -->
		<footer role="contentinfo">
			<jdoc:include type="modules" name="footer" style="none" />
			<p class="pull-right"><a href="#top" id="back-top"><?php echo JText::_('TPL_DCM_BACKTOTOP'); ?></a></p>
			<p>&copy; <?php echo $sitename; ?> <?php echo date('Y');?></p>
			<a href="http://www.jdesignperth.com.au" target="_blank" title="Jdesign Graphic Design Perth"><img class="pull-right jdesign" src="templates/<?php echo $this->template ?>/assets/img/jdesign.png" alt="Jdesign Graphic Design Perth" onmouseout="this.src='templates/<?php echo $this->template ?>/assets/img/jdesign.png'" onmouseover="this.src='templates/<?php echo $this->template ?>/assets/img/jdesignhov.png'" /></a>

		</footer>
		<!-- End Footer -->
	</div>		
	<jdoc:include type="modules" name="debug" style="none" />
	<script src="templates/<?php echo $this->template ?>/assets/js/plugins.js"></script>
    <script src="templates/<?php echo $this->template ?>/assets/js/main.js"></script>
    <script src="templates/<?php echo $this->template ?>/assets/js/vendor/bootstrap.min.js"></script>
</body>
</html>
